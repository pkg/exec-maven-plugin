exec-maven-plugin (3.1.0-1+apertis1) apertis; urgency=medium

  * Move package to development repository. Needed for the Java suite

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Tue, 10 Oct 2023 21:14:59 +0530

exec-maven-plugin (3.1.0-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 05 Oct 2023 16:25:11 +0000

exec-maven-plugin (3.1.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Refreshed the patches
    - Updated the Maven rules
    - New dependency on libmaven-artifact-transfer-java (>= 0.10)
    - New test dependencies: libmockito-java and libslf4j-java
    - Relocate the Maven artifacts for the version 1.6.0
  * Standards-Version updated to 4.6.1
  * Switch to debhelper level 13
  * Use salsa.debian.org Vcs-* URLs
  * Changed the priority from extra to optional

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 13 Dec 2022 10:55:09 +0100

exec-maven-plugin (1.6.0-4.1+apertis1) apertis; urgency=medium

  * Set component to sdk. Move java packages to sdk to avoid building
    for arm architecture.

 -- Vignesh Raman <vignesh.raman@collabora.com>  Tue, 22 Feb 2022 17:22:13 +0530

exec-maven-plugin (1.6.0-4.1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 20:41:22 +0000

exec-maven-plugin (1.6.0-4co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 15 Feb 2021 23:29:28 +0000

exec-maven-plugin (1.6.0-4) unstable; urgency=medium

  * Team upload.
  * Depend on libplexus-component-metadata-java
    instead of libplexus-containers1.5-java

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 06 Sep 2017 08:43:28 +0200

exec-maven-plugin (1.6.0-3) unstable; urgency=medium

  * Team upload.
  * Declare that libexec-maven-plugin-java breaks/replaces (and not conflicts)
    libmaven-exec-plugin-java (<< 1.6.0)

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 05 Sep 2017 17:44:39 +0200

exec-maven-plugin (1.6.0-2) unstable; urgency=medium

  * Team upload.
  * Added the missing Maven rule for plexus-component-annotations

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 05 Sep 2017 08:11:23 +0200

exec-maven-plugin (1.6.0-1) unstable; urgency=medium

  * Team upload.

  [ Emmanuel Bourg ]
  * New upstream release
    - Refreshed the patches
    - New dependency on libcommons-exec-java
    - New build dependency on junit4
  * Depend on libmaven3-core-java instead of libmaven2-core-java
  * Build with the DH sequencer instead of CDBS
  * Standards-Version updated to 4.0.0
  * Use secure Vcs-* URLs
  * Updated the Homepage field
  * Track and download the new releases from GitHub

  [ Andres Mejia ]
  * Recreate packaging using mh_make.
  * Rename package libmaven-exec-plugin-java to libexec-maven-plugin-java.

  [ Gabriele Giacone ]
  * Make VCS-* fields canonical.

  [ tony mancill ]
  * Remove Gabriele Giacone from Uploaders (Closes: #856740)
  * Use debhelper 10

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 26 Jul 2017 10:54:16 +0200

exec-maven-plugin (1.1.1+dfsg-3) unstable; urgency=low

  * Team upload.
  * Add Build-Depends-Indep: libmaven-plugin-tools-java.
  * Update Standards-Version: 3.9.2.

 -- Torsten Werner <twerner@debian.org>  Mon, 12 Sep 2011 20:35:49 +0200

exec-maven-plugin (1.1.1+dfsg-2) unstable; urgency=low

  * Switch to "public package": maintainers are Debian Java Maintainers
  * Standards-Version to 3.8.4

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Thu, 18 Feb 2010 00:35:17 +0100

exec-maven-plugin (1.1.1+dfsg-1) unstable; urgency=low

  * Create a +dfsg tarball:
    - Removed jars from upstream tarball
    - Updated debian/rules, debian/watch, debian/orig-tar.sh accordingly
  * Fixed long description
  * Enabled build tests
  * Updated maintainer email address

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Thu, 18 Feb 2010 00:31:57 +0100

exec-maven-plugin (1.1.1-1) unstable; urgency=low

  * Initial release (Closes: #558330)

 -- Gabriele Giacone <losgarbo@libero.it>  Tue, 01 Dec 2009 15:38:29 +0100
